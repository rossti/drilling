$(document).ready(function () {
    if ($(window).width() <= 768) {
        $('#free-consulting').addClass('hide');
    }
    $( "#callback" ).click(function() {
        $('#free-consulting').toggleClass('hide');
    });
    $( "#cancel-callback" ).click(function() {
        $('#free-consulting').toggleClass('hide');
    });
});